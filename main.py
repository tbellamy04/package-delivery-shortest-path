import heapq
import sys
import datetime
from Graph import Graph, import_distance
from Package import HashTable, Package
from Truck import Truck

# Imports the distance table and builds the address list and distances used to build the graph
table = import_distance()
address_list = []
for row in table:
    address_list.append(row[0])

roads = []
for i in range(0, len(table)):
    end = table[i].index('0')
    for index in range(1, end):
        roads.append((table[i][0], address_list[index-1], float(table[i][index])))


# Builds initialize and build the graph used.
graph = Graph(address_list, roads)

hash_table = HashTable()
truck_1 = Truck("Truck 1", address_list[0], graph)
truck_2 = Truck("Truck 2", address_list[0], graph)
# First load goes on truck 1
first_load = [27, 35, 7, 29, 10, 2, 33, 17]
# Second load is truck 1's second round of deliveries.
# All deliveries in second load are time sensitive
second_load = [1, 30, 8, 4, 40, 32, 31, 26, 25, 6, 28]
# Third load is the first round of deliveries for truck 2
# All deliveries are special restriction deliveries
third_load = [20, 21, 13, 39, 15, 34, 16, 14, 19, 18, 36, 3, 38, 5, 37]
# Fourth load is the second round of deliveries for truck 2
# Deliveries are the remaining packages that need to be delivered
fourth_load = [9, 22, 11, 12, 24, 23]


running = True
while running:
    input_var = input("\n1) Start Delivering\n"
                      "2) Check Progress\n"
                      "3) Search by Status\n"
                      "4) Search by Field\n"
                      "5) Print Miles\n"
                      "6) Exit\n"
                      "Choice: ")
    if int(input_var) == 1:
        while True:
            if truck_1.delivery_round == 1:
                truck_1.load_truck(first_load, address_list, hash_table)
                truck_1.delivery_route()
            elif truck_1.delivery_round == 2:
                truck_1.path.clear()
                truck_1.load_truck(second_load, address_list, hash_table)
                truck_1.delivery_route()
            else:
                print("All truck 1 packages have been delivered.")
                break

            if truck_2.delivery_round == 1:
                truck_2.load_truck(third_load, address_list, hash_table)
                truck_2.delivery_route()
            elif truck_2.delivery_round == 2:
                truck_2.path.clear()
                truck_2.load_truck(fourth_load, address_list, hash_table)
                truck_2.delivery_route()
            else:
                print("All truck 2 packages have been delivered.")
                break

            stop = input("1) Continue Delivering\n"
                         "2) Pause Delivering\n"
                         "Enter Choice: ")
            if int(stop) == 2:
                break
    elif int(input_var) == 2:
        for address in address_list:
            packages = hash_table.get_package(address)

            if packages is not None:
                for package in packages:
                    package.print_package_label()
                    print("\n")
    elif int(input_var) == 3:
        while True:
            search_criteria = input("1) Show delivered packages\n"
                                    "2) Show undelivered packages\n"
                                    "3) Back to main menu\n"
                                    "Enter Choice: ")
            if int(search_criteria) == 1:
                status = "Delivered"
            elif int(search_criteria) == 2:
                status = "At Hub"
            else:
                break

            for address in address_list:
                packages = hash_table.get_package(address)

                if packages is not None:
                    for package in packages:
                        if package.status == status:
                            package.print_package_label()
                            print("\n")
    elif int(input_var) == 4:
        while True:
            search_criteria = input("\n1) Search by Package ID\n"
                                    "2) Search by Address\n"
                                    "3) Search by City\n"
                                    "4) Search by Zip Code\n"
                                    "5) Search by Deadline\n"
                                    "6) Back to Main Menu\n"
                                    "Enter Choice: ")
            if int(search_criteria) == 1:
                id = input("Enter ID: ")
                packages = hash_table.package_search("ID", int(id))
            elif int(search_criteria) == 2:
                address = input("Enter Street Address: ")
                packages = hash_table.package_search("Address", address)
            elif int(search_criteria) == 3:
                city = input("Enter City: ")
                packages = hash_table.package_search("City", city)
            elif int(search_criteria) == 4:
                zip_code = input("Enter Zip Code: ")
                packages = hash_table.package_search("Zip", int(zip_code))
            elif int(search_criteria) == 5:
                deadline = input("Enter deadline in format ##:## AM or PM\n"
                                 "Deadline: ")
                packages = hash_table.package_search("Deadline", deadline)
            elif int(search_criteria) == 6:
                break
            if packages is not None:
                for package in packages:
                    print("\n")
                    package.print_package_label()
            else:
                print("No packages found.")
    elif int(input_var) == 5:
        print("\nTruck 1 miles: %f" % truck_1.miles + "\n"
              "Truck 2 miles: %f" % truck_2.miles + "\n"
              "Total miles: %f" % (truck_1.miles + truck_2.miles))
    elif int(input_var) == 6:
        running = False
