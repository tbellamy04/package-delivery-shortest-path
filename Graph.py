import csv
import heapq

# Imports distance table and returns a 2D list to hold data
def import_distance():
    with open('Distance Table.csv') as csv_file:
        csv_reader = csv.reader(csv_file)
        distance_table = []
        for row in csv_reader:
            distance_table.append(row)
        # Removes row 0 since data is not used
        distance_table.remove(distance_table[0])
        return distance_table


# Used to build the graph structure used in program
class Graph:
    def __init__(self, address_list, roads):
        self.graph = self.make_graph(address_list, roads)

    # Returns a dictionary holding each address in imported distance table and the
    #   connected distances
    def make_graph(self, address_list, roads):
        graph = {address: [] for address in address_list}

        for start, end, distance in roads:
            graph[start].append((end, distance))
            graph[end].append((start, distance))

        return graph

    # Shortest path algorithm that takes in a graph and start address as arguments and
    #   returns a dictionary called distance_table with the
    #   shortest path from the start address to all the addresses that it is connected to
    # Total time complexity of O(N log N)
    def shortest_path(self, start_address):
        # Distance table is a dictionary that will hold the distances to each of the starting addresses neighbors
        distance_table = {}

        # Queue is a priority queue that orders each address by the distance from start.
        # As new paths are found they are added to the queue and then removed when that path
        # is added to the distance table.
        queue = [(0, start_address)]

        # Loop is ran until all paths have been removed from the queue
        while queue:
            # Removes the closest neighbor found from queue then checks if it is currently in the
            #   distance table.
            # If the path has already been found then the path is removed and the next path in
            #   queue is checked.
            # If not the path is added to the distance table then each path connected to it
            #   is checked.
            # Since the queue is in ascending sorted order any path that is not the shortest path
            #   will not be added to the distance table.
            distance1, address = heapq.heappop(queue)
            if address not in distance_table:
                distance_table[address] = distance1
                # Checks to if all the connected paths are in the distance table.
                # If not they are added to the queue.
                for neighbor, distance2 in self.graph[address]:
                    if neighbor not in distance_table:
                        heapq.heappush(queue, (distance1 + distance2, neighbor))

        return distance_table

    # Builds the delivery route for the packages loaded to the the truck
    # Takes in a argument called stops which represents the each address that needs to be
    #   visited on the route.
    # Returns a list of the address objects in the order that the truck will follow
    # Total time complexity of O(N log N)
    def build_path(self, stops):
        path = []

        # Runs the shortest path algorithm to get the first address to visited from the hub
        distance_table = self.shortest_path(stops[0])

        # Assigns the HUB to as the first stop in the path. Hub will always be first
        address = Address(stops[0])
        address.total_distance += 0
        path.append(address)

        # Removes the Hub from stops
        stops.remove(stops[0])
        # Total tracks the distance driven so far on the delivery route
        total = 0
        # Loops through the list of addresses for the packages to be delivered
        # Outer loop time complexity is O(N)
        while stops:
            next_stop = None

            # Loops though the graph nodes until a node that is in the list of stops is found
            # Time complexity is O(N)
            for index in list(distance_table.keys()):
                if index in stops:
                    next_stop = index
                    total = distance_table[index]
                    break

            # Removes the next address to be delivered to from stops.
            # Then creates and appends a Address object that holds the street and total
            # miles driven so far
            stops.remove(next_stop)
            address = Address(next_stop)
            address.total_distance = total
            path.append(address)

            distance_table = self.shortest_path(next_stop)

        return path


# Address class is used to hold the current location of the truck and the distance
#   traveled so far.
class Address:
    def __init__(self, street):
        self.street = street
        self.total_distance = 0


