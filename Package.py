import csv


# Imports package table table
# As each row is read in a new package is built and inserted into the hash table
#   holding the packages
def import_package(hash_table):
    with open('packages.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count != 0:
                id = int(row[0])
                address = row[1]
                city = row[2]
                state = row[3]
                zip = int(row[4])
                deadline = row[5]
                weight = int(row[6])
                notes = row[7]
                new_package = Package(id, address, city, state, zip, deadline,
                                      weight, notes)
                hash_table.insert(new_package)
            line_count += 1


# Chaining hash table
class HashTable:
    # Builds table to will hold the packages.
    # Size is equal to the number of packages to be delivered.
    def __init__(self, size=27):
        self.table = []
        for i in range(size):
            self.table.append([])
        import_package(self)

    # Insert into table method
    def insert(self, package):
        # Hash function used to turn the package address into a integer
        key = hash(package.address)

        # Find bucket by hashing the integer representing the package address
        bucket = key % len(self.table)

        # Set bucket and append a tuple with key and package to the list
        bucket_list = self.table[bucket]
        bucket_list.append((key, package))

    # Takes a in a key argument which is the package address.
    # Returns all packages that have the matching key
    def get_package(self, key):
        # Hash function used to turn the package address into a integer
        key = hash(key)

        # Find bucket by hashing the integer representing the package address
        bucket = key % len(self.table)

        # Go to bucket
        bucket_list = self.table[bucket]

        packages = []
        # Loops through the bucket searching for the key and appending the connected
        #   package if one is found to the packages list
        for index in bucket_list:
            if index[0] == key:
                packages.append(index[1])

        # No packages are found reruns None
        if len(packages) == 0:
            return None

        return packages

    # Loops through hash table and searches for value provided
    # Time complexity is O(N^2)
    def package_search(self, search_field, search_value):
        packages = []
        for bucket in self.table:
            if len(bucket) != 0:
                for index in bucket:
                    if search_field is "ID":
                        if index[1].pid == search_value:
                            packages.append(index[1])
                    elif search_field is "Address":
                        if index[1].address == search_value:
                            packages.append((index[1]))
                    elif search_field is "City":
                        if index[1].city == search_value:
                            packages.append(index[1])
                    elif search_field is "Zip":
                        if index[1].zipCode == search_value:
                            packages.append(index[1])
                    elif search_field is "Deadline":
                        if index[1].deadline == search_value:
                            packages.append(index[1])

        if len(packages) == 0:
            return None

        return packages


class Package:
    def __init__(self, pid, address, city, state, zip_code, deadline,  weight, notes):
        self.pid = pid
        self.address = address
        self.city = city
        self.state = state
        self.zipCode = zip_code
        self.deadline = deadline
        self.weight = weight
        self.notes = notes
        self.status = "At Hub"
        self.delivery_time = None

    def package_loaded(self):
        self.status = "On truck"

    def package_delivered(self, time):
        self.status = "Delivered"
        self.delivery_time = time
        self.print_package_label()

    def print_package_label(self):
        print("Package Label\n"
              "ID: %d" % self.pid + "\n"
              "Address: %s , %s, %s, %s" %
              (self.address, self.city, self.state, self.zipCode) + "\n" 
              "Deadline: %s" % self.deadline + "\n"
              "Weight: %s" % self.weight + "\n"
              "Notes: %s" % self.notes + "\n"                             
              "Status: %s" % self.status + "\n"
              "Delivery Time: %s" % self.delivery_time)
