import datetime


class Truck:
    def __init__(self, name, hub, graph):
        self.name = name
        self.packages = []
        self.location = "At Hub"
        self.path = []
        self.max = 16
        self.delivery_round = 1
        self.current_time = datetime.datetime(100, 1, 1, 8, 00, 00)
        self.miles = 0
        self.home = hub
        self.graph = graph

    def load_package(self, new_package):
        new_package.package_loaded()
        self.packages.append(new_package)

    def delivery(self, package):
        package.package_delivered(self.current_time.time())

    def delivery_round_complete(self):
        self.delivery_round += 1

    # Calculates the time it takes to travel to each location and add miles traveled to the trucks
    #   total miles driven
    def travel_to_location(self, miles):
        speed = 18
        if miles != 0:
            secs = (miles / speed) * 60 * 60
            self.current_time = self.current_time + datetime.timedelta(0, secs)
        self.miles += miles

    # Sends the truck back to the hub once all packages on the truck a delivered.
    def travel_to_hub(self):
        self.path.clear()
        path = [self.location.street, self.home]
        self.path = self.graph.build_path(path)
        self.delivery_round += 1

        for i in self.path:
            self.location = i
            self.travel_to_location(i.total_distance)
        print("Traveling to Hub\n"
              "Arrival Time: %s" % self.current_time.time() + "\n"
              "Total Miles: %f" % self.miles + "\n")

    # Updates trucks current path if a package address is updated
    def update_path(self):
        path = []
        path.append(self.location.street)
        for package in self.packages:
            path.append(package.address)

        self.path = self.graph.build_path(path)

    # Loads packages to the truck and builds path
    def load_truck(self, package_ids, address_list, package_table):
        self.path.append(self.home)
        for address in address_list:
            if len(self.packages) == self.max:
                break

            packages = package_table.get_package(address)

            if packages is not None:
                if (len(self.packages) + len(packages)) < self.max:
                    for package in packages:
                        if package.pid in package_ids:
                            self.load_package(package)

                            if package.address not in self.path:
                                self.path.append(address)

        self.path = self.graph.build_path(self.path)

    # Controls the trucks deliveries all packages.
    # Once are packages have been delivered travel to hub is called.
    def delivery_route(self):
        delivering = True
        print("Delivering %s packages" % self.name)
        idx_1 = 0
        not_updated = True
        while delivering:
            if self.current_time.time() > datetime.datetime(100, 1, 1, 10, 20, 00).time():
                if not_updated:
                    for package in self.packages:
                        if package.pid == 9:
                            package.address = "410 S State St."
                            package.city = "Salt Lake City"
                            package.state = "UT"
                            package.zipCode = "84111"
                    not_updated = False
                    self.update_path()

            if idx_1 != len(self.path):
                self.location = self.path[idx_1]
                self.travel_to_location(self.location.total_distance)
                idx_1 += 1

                index = 0
                while index < len(self.packages):
                    if self.packages[index].address == self.location.street:
                        self.delivery(self.packages[index])
                        print("Total Miles: %f" % self.miles + "\n")
                        self.packages.remove(self.packages[index])
                    else:
                        index += 1

            if idx_1 == len(self.path):
                self.travel_to_hub()
                delivering = False

